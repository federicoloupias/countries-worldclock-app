## Available Scripts

To create this app, I worked with this versions:

- Node: v14.15.4
- Npm: v6.14.10
- Yarn: v1.22.5
- Docker : version 20.10.6, build 370c289.


To build the app, you can create an image with docker

### `docker build -f Dockerfile.prod -t countries-app-image:latest .`

Then run it ! :)

### `docker run -it -p 8080:80 --rm countries-app-image:latest`


Open [http://localhost:8080](http://localhost:8080) to view it in the browser.


Thanks! Have a good day. :D