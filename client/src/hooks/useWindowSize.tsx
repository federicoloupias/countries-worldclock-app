import { useEffect, useState } from 'react';

export const useWindowSize = () => {

  const [windowWidthSize, setWindowWidthSize] = useState( window.innerWidth );

  useEffect(() => {

    function handleResize() {
      setWindowWidthSize( window.innerWidth );
    }

    window.addEventListener("resize", handleResize);

    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowWidthSize;
}
