import React, {useEffect} from 'react';
import './App.css';
import { useDispatch, useSelector } from "react-redux";
import { RootStore } from "./Store";
import { GetCountries } from "./redux/actions/countries/CountriesActions";


import { InputBar } from './components/InputBar';
import { Grid } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { CountryCard } from './components/CountryCard';
import { CountryType } from './redux/actions/countries/CountriesActionTypes';
import { LoadingCard } from './components/LoadingCard';
import { useWindowSize } from './hooks/useWindowSize';



function App() {
  const dispatch = useDispatch();
  const countriesState = useSelector((state: RootStore) => state.countries);
  const width = useWindowSize();

  useEffect(() => {
    dispatch(GetCountries());
  }, [dispatch]);



  const renderItem = ( { id, name, day, hour }: CountryType ) => {
    return (
     (width<= 575)
       ?
       <Grid item xs>
           <CountryCard key={ id } id={ id } name={ name } day= { day } hour={ hour }/>
       </Grid>

       :

       <Grid item xs={ 4 }>
           <CountryCard key={ id } id={ id } name={ name } day= { day } hour={ hour }/>
       </Grid>
    )
}



  return (
    <div className="App">
      {

      ( countriesState.getCountries && !countriesState.loadingCountries )
      ?

      <InputBar countries= { countriesState.countries } />

      :

      ( countriesState.loadingCountries )

      ?

      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12}>
            <Alert severity="info">
                <AlertTitle>Info</AlertTitle>
                Cargando los paises. <strong>Espere unos segundos</strong>
            </Alert>
        </Grid>
      </Grid>

      :

      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12}>
            <Alert severity="warning">
                <AlertTitle>Info</AlertTitle>
                No se han podido cargar los paises. Recargue la página <strong>(F5)</strong>
            </Alert>
        </Grid>
      </Grid>

      }

      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={3}>
          {
            countriesState.countriesToShow.map( country => renderItem(country) )
          }
          <Grid item xs={ 4 }>

          {

          (countriesState.loadingCountry)

          ?

            <LoadingCard />

          :

          <div/>

          }
          </Grid>
        </Grid>
      </Grid>


    </div>
  );
}

export default App;
