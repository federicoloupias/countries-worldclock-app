import React, { useState } from 'react';

import { GetCountryData } from '../redux/actions/countries/CountriesActions';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, IconButton, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { RootStore } from '../Store';
import { CountryType } from '../redux/actions/countries/CountriesActionTypes';


interface InputBarProps {
    countries: string []
}


export const InputBar: React.FC<InputBarProps> = ( { countries } ) => {


    const [ countrySelected , setCountrySelected] = useState('');
    const [ openDialog, setOpenDialog ] = useState(false);
    const countriesState = useSelector((state: RootStore) => state.countries);

    const dispatch = useDispatch();

    const search = (nameKey: string, myArray: CountryType []) => {
        var result = -1;
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                result = i;
                return result;
            }
        }
        return result;
    }

    const addCountryClick = () => {

        let isCountryShown = search( countrySelected, countriesState.countriesToShow)
        if(isCountryShown === -1){
            if(countrySelected){
                dispatch(GetCountryData(countrySelected));
            }
        } else  {
            setOpenDialog(true);
        }
    }

    const handleClose = () => {
        setOpenDialog(false);
      };


    return (
        <>
            <Grid container spacing={3} alignItems="center">
                <Grid item xs={10}>
                <Autocomplete
                id="combo-box-demo"
                options={ countries }
                getOptionLabel={(option) => option}
                style={{ width: '100%', padding: 10}}
                renderInput={(params) => <TextField {...params} label="Selecciona Pais" variant="outlined" />}
                inputValue={countrySelected}
                onInputChange={(event, newInputValue) => {
                    setCountrySelected(newInputValue);
                }}
                />
                </Grid>
                <Grid item xs={2} >
                    <IconButton onClick={addCountryClick} >
                        <AddIcon fontSize="large" />
                    </IconButton>
                </Grid>
            </Grid>


            <Dialog
                open={openDialog}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Por favor ingrese un nuevo país."}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    El pais que has seleccionado - { countrySelected } - ya se encuentra en la lista.
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Entendido
                </Button>
                </DialogActions>
            </Dialog>
        </>
    )
}
