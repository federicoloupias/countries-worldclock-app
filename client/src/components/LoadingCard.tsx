import { Card, CircularProgress, Typography } from '@material-ui/core';
import React from 'react'
import CardContent from '@material-ui/core/CardContent';

export const LoadingCard = () => {
    return (
        <div>
            <Card variant="outlined" >
                    <CardContent style={{
                        textAlign:'center'
                    }}>
                        <CircularProgress />
                            <h5> - - - - - / - - - - - </h5>
                        <Typography  component="p">
                         - -/- -/- - - -
                        <br />
                        </Typography>
                    </CardContent>
                </Card>

        </div>
    )
}
