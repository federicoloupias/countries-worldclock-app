import React from 'react';

import { Box, Card, CardContent, IconButton, Typography } from '@material-ui/core';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

import { DeleteCountryData, ReloadCountryData } from '../redux/actions/countries/CountriesActions';
import { useDispatch, useSelector } from 'react-redux';
import useInterval from '../hooks/useInterval';
import { RootStore } from '../Store';


interface CountryCardProps {
    id: number,
    name: string,
    day: string,
    hour: string,
}


export const CountryCard: React.FC<CountryCardProps> = ({ id, name, day, hour } : CountryCardProps) => {

    const dispatch = useDispatch();

    const countriesState = useSelector((state: RootStore) => state.countries);

    useInterval(() => {
        var index = countriesState.isFetchingCountriesToShow.findIndex(i => i.idCountry === id);
        if(!countriesState.isFetchingCountriesToShow[index].isFetchingCountry){
            console.log('no esta cargando data country')
            let country = { id, name, day, hour };
            if(country){
                dispatch(ReloadCountryData(country));
            }
        }
        console.log('cargando data country -', name)
      }, 5000);


    const deleteCountryClick = (idCountry: number) => {
        dispatch(DeleteCountryData(idCountry));
    }

    return (
        <>
                <Card variant="outlined" >
                    <CardContent style={{
                        textAlign:'center'
                    }}>
                        <Box textAlign="right">
                            <IconButton
                            onClick={() => deleteCountryClick(id)} size='small'>
                            <DeleteOutlinedIcon />
                            </IconButton>
                        </Box>
                            <h5>{ name }</h5>
                        <Typography  component="p">
                        { day }
                        <br />
                        { hour }
                        </Typography>
                    </CardContent>
                </Card>

        </>
    )
}
