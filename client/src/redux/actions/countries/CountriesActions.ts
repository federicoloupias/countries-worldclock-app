import {Dispatch} from "redux";
import { COUNTRIES_FAIL,
         COUNTRIES_LOADING,
         COUNTRIES_SUCCESS,
         COUNTRY_FAIL,
         COUNTRY_LOADING,
         COUNTRY_SUCCESS,
         COUNTRY_DELETE_SUCCESS,
         COUNTRY_RELOAD_SUCCESS,
         IS_FETCHING_COUNTRY,
         CountriesDispatchTypes,
         CountryType
        } from "./CountriesActionTypes";
import axios from "axios";

export const GetCountries = () => async (dispatch: Dispatch<CountriesDispatchTypes>) => {
  try {
    dispatch({
      type: COUNTRIES_LOADING
    })

    const res = await axios.get(`http://worldtimeapi.org/api/timezones`);

    dispatch({
      type: COUNTRIES_SUCCESS,
      payload: res.data
    })

  } catch(e) {
    dispatch({
      type: COUNTRIES_FAIL
    })
  }
};

const formatAMPM = (date: Date) => {
  var hours = date.getHours();
  var minutes: number | string = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

const handleDateFormat = (data: any) => {
  var date = null;
  var dateFull = [];
    if (data) {

      if( data.client_ip ){
          var dateToFormat =  data.datetime.split(".");
          date = new Date( dateToFormat[0] );
          dateFull[0] = date.getDate() + '/' + ( date.getMonth() + 1 ) + '/' + date.getFullYear();
          dateFull[1] = formatAMPM(date);
      }
  }
  return dateFull;
}

const handleCountryData = (data: any) => {
    let newCountry: CountryType
    const resp = handleDateFormat(data)
    let id = Date.now();
        newCountry = {
            id: id,
            name: data.timezone,
            day: resp[0],
            hour: resp[1]
        }
    return newCountry;
}

const handleReloadCountryData = (data: any, idCountry: number) => {
  let newCountry: CountryType
  const resp = handleDateFormat(data);
      newCountry = {
          id: idCountry,
          name: data.timezone,
          day: resp[0],
          hour: resp[1]
      }
  return newCountry;
}

export const GetCountryData = (countryName: string) => async (dispatch: Dispatch<CountriesDispatchTypes>) => {
  try {
    dispatch({
      type: COUNTRY_LOADING
    })


    const res = await axios.get(`http://worldtimeapi.org/api/timezone/${countryName}`);

    console.log('RES- AXIOS - ', res);

    const newCountry = handleCountryData(res.data);

    if( !newCountry.id || !newCountry.name || !newCountry.day || !newCountry.hour ){
      dispatch({
            type: COUNTRY_FAIL
          })
    }

    if( newCountry ) {
      dispatch({
        type: COUNTRY_SUCCESS,
        payload: newCountry
      })
    }

  } catch(e) {
    dispatch({
      type: COUNTRY_FAIL
    })
  }
};

export const DeleteCountryData = (idCountry: number) => async (dispatch: Dispatch<CountriesDispatchTypes>) => {

    dispatch({
      type: COUNTRY_DELETE_SUCCESS,
      payload: idCountry
    })
};

export const ReloadCountryData = (country: CountryType) => async (dispatch: Dispatch<CountriesDispatchTypes>) => {

  let countryFetchingPending = {
    idCountry: country.id,
    isFetchingCountry: true
  }

  let countryFetchingDone = {
    idCountry: country.id,
    isFetchingCountry: false
  }

  try {

    dispatch({
      type: IS_FETCHING_COUNTRY,
      payload: countryFetchingPending
    })
    const res = await axios.get(`http://worldtimeapi.org/api/timezone/${country.name}`);

    const newCountry = handleReloadCountryData(res.data, country.id);

    if( !newCountry.id || !newCountry.name || !newCountry.day || !newCountry.hour ){
      dispatch({
        type: IS_FETCHING_COUNTRY,
        payload: countryFetchingDone
      })
      dispatch({
            type: COUNTRY_FAIL
          })
    }

    if( newCountry ){
      dispatch({
        type: IS_FETCHING_COUNTRY,
        payload: countryFetchingDone
      })
      dispatch({
        type: COUNTRY_RELOAD_SUCCESS,
        payload: newCountry
      })
    }

  } catch(e) {
    dispatch({
      type: IS_FETCHING_COUNTRY,
      payload: countryFetchingDone
    })
    dispatch({
      type: COUNTRY_FAIL
    })
  }
};
