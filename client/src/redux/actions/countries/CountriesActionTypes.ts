export const COUNTRIES_LOADING = "COUNTRIES_LOADING";
export const COUNTRIES_FAIL = "COUNTRIES_FAIL";
export const COUNTRIES_SUCCESS = "COUNTRIES_SUCCESS";

export const COUNTRY_LOADING = "COUNTRY_LOADING";
export const COUNTRY_FAIL = "COUNTRY_FAIL";
export const COUNTRY_SUCCESS = "COUNTRY_SUCCESS";
export const COUNTRY_DELETE_SUCCESS = "COUNTRY_DELETE_SUCCESS";
export const COUNTRY_RELOAD_SUCCESS = "COUNTRY_RELOAD_SUCCESS";

export const FETCHING_COUNTRY = "FETCHING_COUNTRY";
export const IS_FETCHING_COUNTRY = "IS_FETCHING_COUNTRY";

export interface CountriesLoading {
  type: typeof COUNTRIES_LOADING
}

export interface CountriesFail {
  type: typeof COUNTRIES_FAIL
}

export interface CountriesSuccess {
  type: typeof COUNTRIES_SUCCESS,
  payload: string[]
}


export type CountryType = {
  id: number,
  name: string,
  day: string,
  hour: string
}

export interface CountryLoading {
  type: typeof COUNTRY_LOADING
}

export interface CountryFail {
  type: typeof COUNTRY_FAIL
}

export interface CountrySuccess {
  type: typeof COUNTRY_SUCCESS,
  payload: CountryType
}

export interface CountryDeleteSuccess {
  type: typeof COUNTRY_DELETE_SUCCESS
  payload: number
}

export interface CountryReloadSuccess {
  type: typeof COUNTRY_RELOAD_SUCCESS
  payload: CountryType
}

export type FetchingCountryType = {
  idCountry: number,
  isFetchingCountry: boolean
}

export interface isFetchingCountry {
  type: typeof IS_FETCHING_COUNTRY
  payload: FetchingCountryType
}





export type CountriesDispatchTypes = CountriesLoading |
                                     CountriesFail |
                                     CountriesSuccess |
                                     CountryLoading |
                                     CountryFail |
                                     CountrySuccess |
                                     CountryDeleteSuccess |
                                     CountryReloadSuccess |
                                     isFetchingCountry
