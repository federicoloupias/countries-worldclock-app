import { FetchingCountryType } from '../../actions/countries/CountriesActionTypes';

import {
  COUNTRIES_FAIL,
  COUNTRIES_LOADING,
  COUNTRIES_SUCCESS,
  COUNTRY_FAIL,
  COUNTRY_LOADING,
  COUNTRY_SUCCESS,
  COUNTRY_DELETE_SUCCESS,
  COUNTRY_RELOAD_SUCCESS,
  IS_FETCHING_COUNTRY,
  CountryType,
  CountriesDispatchTypes
} from "../../actions/countries/CountriesActionTypes";

interface DefaultCountriesStateI {
  getCountries?: boolean,
  loadingCountries?: boolean,
  loadingCountry?: boolean,
  isFetchingCountriesToShow: FetchingCountryType []
  countriesToShow: CountryType []
  countries: string[]
}

const defaultState: DefaultCountriesStateI = {
  getCountries: false,
  loadingCountries: false,
  loadingCountry: false,
  isFetchingCountriesToShow: [],
  countriesToShow: [],
  countries: []
};

const handleFetchingCountriesToShow = (arrToCompare: FetchingCountryType[], idNewCountry: number) => {
  let newFetchingCountriesToShow = [...arrToCompare]
  if( arrToCompare.length > 0 ){

    // Si ya se estan mostrando Countries, me fijo si ya existe
    // un Country que este recargando su data, sino existe lo agrego
    // si ya existe, lo dejo igual
    var indexFetching = newFetchingCountriesToShow.findIndex(i => i.idCountry === idNewCountry);
    if( indexFetching === -1 ){

      // Se estan mostrando countries pero el country por el cual se esta consultando
      // no existe en los countries que estan pidiendo data, lo agrego
      let newFetchingCountry = {
        idCountry: idNewCountry,
        isFetchingCountry: false
      }
      newFetchingCountriesToShow.push(newFetchingCountry)
    }
  } else {

    // Primera vez que se agrega un country para mostrar
    let newFetchingCountry = {
      idCountry: idNewCountry,
      isFetchingCountry: false
    }
    newFetchingCountriesToShow.push(newFetchingCountry);
  }
  return newFetchingCountriesToShow;
}



const countriesReducer = (state: DefaultCountriesStateI = defaultState, action: CountriesDispatchTypes) : DefaultCountriesStateI => {
  switch (action.type) {
    case COUNTRIES_FAIL:
      return {
        loadingCountries: false,
        getCountries: false,
        loadingCountry: state.loadingCountry,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        countries: [...state.countries ],
        countriesToShow: [...state.countriesToShow ]
      }
    case COUNTRIES_LOADING:
      return {
        loadingCountries: true,
        getCountries: state.getCountries,
        loadingCountry: state.loadingCountry,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        countries: [...state.countries ],
        countriesToShow: [...state.countriesToShow ]
      }
    case COUNTRIES_SUCCESS:
      return {
        loadingCountries: false,
        getCountries: true,
        loadingCountry: state.loadingCountry,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        countries: action.payload,
        countriesToShow: [...state.countriesToShow ]
      }
      case COUNTRY_FAIL:
      return {
        loadingCountry: false,
        loadingCountries: state.loadingCountries,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        getCountries: state.getCountries,
        countries: [...state.countries ],
        countriesToShow: [...state.countriesToShow ]
      }
    case COUNTRY_LOADING:
      return {
        loadingCountry: true,
        loadingCountries: state.loadingCountries,
        getCountries: state.getCountries,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        countries: [...state.countries ],
        countriesToShow: [...state.countriesToShow ]
      }
    case COUNTRY_SUCCESS:

      var newFetchingCountriesToShow = handleFetchingCountriesToShow([...state.isFetchingCountriesToShow ], action.payload.id)

      return {
        loadingCountry: false,
        loadingCountries: state.loadingCountries,
        getCountries: state.getCountries,
        isFetchingCountriesToShow: [...newFetchingCountriesToShow ],
        countries: [...state.countries ],
        countriesToShow: [...state.countriesToShow, action.payload ]
      }
    case COUNTRY_DELETE_SUCCESS:
      return {
        loadingCountry: state.loadingCountry,
        loadingCountries: state.loadingCountries,
        getCountries: state.getCountries,
        isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
        countries: [...state.countries ],
        countriesToShow: state.countriesToShow.filter(({id}) => id !== action.payload)
      }
      case COUNTRY_RELOAD_SUCCESS:
        let newCountriesToShow = [...state.countriesToShow ]
        var index = newCountriesToShow.findIndex(i => i.id === action.payload.id);
        if ( index !== -1 ) {
          newCountriesToShow[index] = action.payload;
          return {
            loadingCountry: state.loadingCountry,
            loadingCountries: state.loadingCountries,
            getCountries: state.getCountries,
            isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
            countries: [...state.countries ],
            countriesToShow: [...newCountriesToShow ]
          }
        } else {
          return {
            loadingCountry: state.loadingCountry,
            loadingCountries: state.loadingCountries,
            isFetchingCountriesToShow: [...state.isFetchingCountriesToShow ],
            getCountries: state.getCountries,
            countries: [...state.countries ],
            countriesToShow: [...state.countriesToShow ]
          }
        }
        case IS_FETCHING_COUNTRY:
        let newFetchingCountriesToShow1 = [...state.isFetchingCountriesToShow ]
        var indexFetching = newFetchingCountriesToShow1.findIndex(i => i.idCountry === action.payload.idCountry);
        if ( ~indexFetching ){
          newFetchingCountriesToShow1[indexFetching] = action.payload;
          return {
            loadingCountry: state.loadingCountry,
            loadingCountries: state.loadingCountries,
            getCountries: state.getCountries,
            isFetchingCountriesToShow: [...newFetchingCountriesToShow1 ],
            countries: [...state.countries ],
            countriesToShow: [...state.countriesToShow ]
          }
        } else {
          newFetchingCountriesToShow1.push(action.payload);
          return {
            loadingCountry: state.loadingCountry,
            loadingCountries: state.loadingCountries,
            getCountries: state.getCountries,
            isFetchingCountriesToShow: [...newFetchingCountriesToShow1 ],
            countries: [...state.countries ],
            countriesToShow: [...state.countriesToShow ]
          }
        }

    default:
      return state
  }
};


export default countriesReducer;