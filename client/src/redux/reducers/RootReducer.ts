import {combineReducers} from "redux";
import countriesReducer from "./countries/CountriesReducer";

const RootReducer = combineReducers({
  countries: countriesReducer
});

export default RootReducer